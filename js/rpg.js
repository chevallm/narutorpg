var map = new Map("pokemon");
var obstacle = new Map("pokemonO");

var joueur = new Personnage("Naruto", "naruto.png", 7, 6, DIRECTION.HAUT);
var sakura = new Personnage("Sakura","sakura.png", 8, 3, DIRECTION.HAUT);
var sasuke = new Personnage("Sasuke","sasuke.png", 8, 6, DIRECTION.BAS);
var kakashi = new Personnage("Kakashi","kakashi.png", 14 ,6, DIRECTION.GAUCHE);
//map.addPersonnage(joueur);
obstacle.addPersonnage(joueur);
obstacle.addPersonnage(sakura);
obstacle.addPersonnage(sasuke);
obstacle.addPersonnage(kakashi);



window.onload = function() {

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');   

    // Gestion du clavier

	window.onkeydown = function(event) {
		var e = event || window.event;
		var key = e.which || e.keyCode;
	    switch(key) {

    	case 38 : case 122 : case 90 : // Flèche haut, z, Z
        	joueur.deplacer(DIRECTION.HAUT, obstacle);
        	break;

    	case 40 : case 115 : case 83 : // Flèche bas, s, S
        	joueur.deplacer(DIRECTION.BAS, obstacle);
        	break;

    	case 37 : case 113 :case 81 :// Flèche gauche, q, Q
        	joueur.deplacer(DIRECTION.GAUCHE, obstacle);
        	break;

    	case 39 : case 100 : case 68 : // Flèche droite, d, D
        	joueur.deplacer(DIRECTION.DROITE, obstacle);
        	break;

        case 32 : // Espace
            joueur.parler(joueur.direction ,obstacle, ctx);
            break;


    		default : 
        	//alert(key);
        	// Si la touche ne nous sert pas, nous n'avons aucune raison de bloquer son comportement normal.

        return false;

}
	}

    canvas.width  = map.getLargeur() * 32;
    canvas.height = map.getHauteur() * 32;



    setInterval(function() {
    map.dessinerMap(ctx);
    obstacle.dessinerMap(ctx);

    ctx.font = "10px Arial";
    ctx.fillStyle = "rgb(0,0,0)";
    ctx.fillText("Naruto RPG 0.1", 10, 20);
	}, 20);


}
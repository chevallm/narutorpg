function Map(nom) {
	
	// Création de l'objet XmlHttpRequest
	var xhr = getXMLHttpRequest();

	// Liste des personnages présents sur le terrain.
	this.personnages = new Array();  

	// Chargement du fichier
	xhr.open("GET", './maps/' + nom + '.json', false);
	xhr.send(null);
	if(xhr.readyState != 4 || (xhr.status != 200 && xhr.status != 0)) // Code == 0 en local
	    throw new Error("Impossible de charger la carte nommée \"" + nom + "\" (code HTTP : " + xhr.status + ").");
	var mapJsonData = xhr.responseText;
	var mapData = JSON.parse(mapJsonData);
	this.tileset = new Tileset(mapData.tileset);
	this.terrain = mapData.terrain;
}



// Pour ajouter un personnage

Map.prototype.addPersonnage = function(perso) {
    this.personnages.push(perso);
}

Map.prototype.getHauteur = function() {
    return this.terrain.length;
}

Map.prototype.getLargeur = function() {
    return this.terrain[0].length;
}

function numSort(arr) {
  return arr.sort(function(a, b) { return +a - +b; });
}


Map.prototype.dessinerMap = function(context) {

    for(var i = 0, l = this.terrain.length ; i < l ; i++) {
        var ligne = this.terrain[i];
        var y = i * 32;
        for(var j = 0, k = ligne.length ; j < k ; j++) {
            this.tileset.dessinerTile(ligne[j], context, j * 32, y); 
        }
    }
	for(var xmap = 0; xmap < this.terrain.length ; xmap ++)	{
		for(var i = 0, l = this.personnages.length ; i < l ; i++) {
			if(this.personnages[i].y == xmap) {
				this.personnages[i].dessinerPersonnage(context);
				if(i != 0)	{
					this.terrain[this.personnages[i].y][this.personnages[i].x] = 0;
				}
								
			}				
		}
	}
}
